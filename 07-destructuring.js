// Destructuring

/* ES5 */
var person = {
  name: "Rick",
  age: 30,
};
console.log(
  "His name is " + person.name + " and he is " + person.age + " years old"
);

/* ES6 */
let person1 = {
  firstname: "Rick",
  phone: 123456789,
};

const { firstname, phone } = person1;
console.log(`His name is ${firstname} and his phone number is ${phone}`);

// ------------------------------------

let person2 = {
  lastname: "Rock",
  email: "rick@mail.com",
};

function getPerson() {
  return person2;
}

const { lastname, email } = getPerson();
console.log(`His name is ${lastname} and his email is ${email}`);

// ------------------------------------

let persons = [
  {
    name: "Rick",
    email: "rick@mail.com",
  },
  {
    name: "Karl",
    email: "karl@mail.com",
  },
];

var [_, secondPerson] = persons;
console.log(secondPerson);
