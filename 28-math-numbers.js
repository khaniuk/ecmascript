// Math and Numbers

/* ES6 */

// Math

// Returns the integer part of a
let a = Math.trunc(4.932);
console.log(`Trunc: ${a}`);

// returns if a is negative, null or positive
a = Math.sign(4);
console.log(`Sign: ${a}`);

a = Math.sign(-4);
console.log(`Sign: ${a}`);

// Numers
let x = Number.EPSILON;
console.log(`EPSILON: ${x}`);

x = Number.MIN_SAFE_INTEGER;
console.log(`MIN_SAFE_INTEGER: ${x}`);

x = Number.MAX_SAFE_INTEGER;
console.log(`MAX_SAFE_INTEGER: ${x}`);

// Method returns true if the argument is an integer
let y = Number.isInteger(35);
console.log(`35 Is integer: ${y}`);

y = Number.isInteger(35);
console.log(`35.5 Is integer: ${y}`);

// A safe integer is an integer that can be exactly represented as a double precision number
let z = Number.isSafeInteger(40);
console.log(`40 Is safe integer: ${z}`);

z = Number.isSafeInteger(40.5);
console.log(`40.5 Is safe integer: ${z}`);

// Check isFinite() and isNaN()

/* ES7 ECMAScript 2016 */
// Exponentiation Operator

let base = 5;
let result = base ** 2;
console.log(`Exponentiation: ${result}`);

// Equals result
let eq = 5;
eq **= 2; // result 25
console.log(`Exponentiation: ${eq}`);
