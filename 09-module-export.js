// Export

/* ES6 */

function sum(x, y) {
  return x + y;
}

export { sum };
