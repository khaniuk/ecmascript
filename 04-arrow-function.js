// Arrow function

/* ES5 */
function sum(x, y) {
  const result = x + y;
  return result;
}
console.log("ES5 sum ", sum(2, 4));

/* ES6 */
const subtraction = (x, y) => {
  return x - y;
};
console.log("ES6 subtraction ", subtraction(5, 3));

// ------------------------------------

const multiplication = (x, y) => x * y;
console.log("ES6 multiplication", multiplication(3, 5));

// ------------------------------------

const square = (x) => x * x;
console.log("ES6 square ", square(3));
