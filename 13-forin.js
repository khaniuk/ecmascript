// forIn

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
];

/* ES5 */

for (const key in persons) {
  const person = persons[key];
  console.log(`${key} : ${person.name}`);
}

// Check Array.keys(); return keys iterable
