// Template Literal

const person = { name: "Rick", age: 30 };

/* ES5 */
console.log(
  "ES5 His name is " + person.name + " and he is " + person.age + " years old"
);

// Break \n
console.log(
  "ES5 His name is " + person.name + "\nand he is " + person.age + " years old"
);

/* ES6 */
// Use back-tic symbol
console.log(`ES6 His name is ${person.name} and he is ${person.age} years old`);

console.log(`ES6 His name is ${person.name}
and he is ${person.age} years old`);
