// Objects shorthand

let firstname = "Rick";
let age = 30;

/* ES5 */
var onePerson = {
  firstname: firstname,
  age: age,
};

console.log(
  `ES5 His name is ${person.firstname} and he is ${person.age} years old`
);

/* ES6 */
let person = {
  firstname,
  age,
};

console.log(
  `ES6 His name is ${person.firstname} and he is ${person.age} years old`
);
