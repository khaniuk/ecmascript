// Array reduce

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5 */

// Sum all values (age)
let result = persons.reduce((acc, person) => acc + person.age, 0);
console.log(result);

let result2 = persons.reduceRight((acc, person) => acc + person.age, 0);
console.log(result2);
