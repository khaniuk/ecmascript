// String functions

/* ES8 ECMAScript 2017 */
const addValue = 0;
const lengthValue = 4;

let strValue = "5";
let result = strValue.repeat(strValue);
console.log(`Result repeat: ${result}`);

result = strValue.padStart(lengthValue, addValue);
console.log(`Result padStart: ${result}`);

result = strValue.padEnd(lengthValue, addValue);
console.log(`Result padStart: ${result}`);

/* ES10 ECMAScript 2019 */

// trim start
let hello = "           hello world!";
console.log(hello);
console.log(hello.trimStart());

// trim end
hello = "hello world!           ";
console.log(hello);
console.log(hello.trimEnd());
