// Array from

const numbers = "123456789";

/* ES6 */

// Returns an Array object from any object with a length property or any iterable object.
let result = Array.from(numbers); //DOM
console.log(result);

// Show tabla
console.table(result);
