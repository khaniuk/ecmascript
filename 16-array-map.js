// Array map

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
];

/* ES5 */

let result = persons.forEach((person) => person.age * 2);
console.log(result);

let ages = persons.map((person) => person.age * 2);
console.log(ages);
