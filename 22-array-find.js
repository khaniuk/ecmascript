// Array find

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5, ES6 */

// Returns the element of the first array element that passes a test function
let result = persons.find((person) => person.age === 35);
console.log(result); // Then true : else undefined

// Check findIndex()
