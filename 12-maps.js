// Maps

/* ES6 */

let person = new Map();
console.log(person);

// Set element
person.set("Rick", 35);
person.set("Karl", 20);
person.set("Carol", 32);
console.log(person);

// Get value from key
console.log(`Get value: ${person.get("Rick")}`);

// Delete element
person.delete("Carol");
console.log(person);

// Compare element by key
console.log(person.has("Rick"));

// Get entries fron Map
console.log(person.entries());

// Get size elements
console.log(person.size);
