// Promise

const division = (x, y) => {
  return new Promise((resolve, reject) => {
    /* if (y > 0) {
      resolve(x / y);
    } else {
      reject("Error, not resolve");
    } */
    try {
      resolve(x / y);
    } catch (err) {
      reject(err);
    }
  });
};

division(8, 2)
  .then((result) => console.log(result))
  .catch((error) => console.log(error));
