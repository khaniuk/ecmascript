// Array indexOf

const person = {
  name: "Judith",
  age: 10,
};

let persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5 */

// Search an array for an element value and returns its position
let result = persons.indexOf(person);
console.log(result);

persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  person,
];

let position = persons.indexOf(person);
console.log(position);
