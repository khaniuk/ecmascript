// Default Params

/* ES5 */
function user(name, age) {
  let name = name || "Karl";
  let age = age || 20;

  console.log(`ES5 His name is ${name} and he is ${age} years old`);
}
user();
user("Rick", 35);

/* ES6 */
function person(name = "Karl", age = 20) {
  console.log(`ES6 His name is ${name} and he is ${age} years old`);
}
person();
person("Rick", 35);
