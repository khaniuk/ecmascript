// forEach

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
];

/* ES5 */

persons.forEach((person, index) => console.log(`${index} : ${person.name}`));

persons.forEach((person) => {
  console.log(person.name);
});
