// Promise

/* ES7 ECMAScript 2018 */

let myPromise = new Promise();

myPromise.then(console.log("success"));
myPromise.catch(console.log("error"));
myPromise.finally(console.log("finaly"));
