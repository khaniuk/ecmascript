// Array every

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5 */

// Check if all ages are over 18
let result = persons.every((person) => person.age > 18);
console.log(result);
