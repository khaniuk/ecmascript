// Sets

/* ES6 */

const person = new Set();
console.log(person);

// Adding element
person.add("Rick");
person.add(20);
person.add("walking");
console.log(person);

// Delete element
person.delete("walking");
console.log(person);

// Compare element
console.log(person.has("Rick"));

// Get values fron Set
console.log(person.values());

// Get size elements
console.log(person.size);
