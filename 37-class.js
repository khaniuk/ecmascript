// Class

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  getName() {
    return this.name;
  }
}

const person = new Person("Rick", 35);
// Result object person
console.log(person);

// Result reference property
console.log(person.name);

// Result use getName
console.log(person.getName());
