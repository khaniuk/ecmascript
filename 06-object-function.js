// Object with function

let firstname = "Rick";
let age = 30;

/* ES5 */
// Not example

/* ES6 */
const person = {
  firstname,
  age,
  walk: () => console.log("walking"),
};

console.log(`The ${person.firstname} and is `);
person.walk();
