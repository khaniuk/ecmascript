// Spread operator

let numbers = Array(1, 2, 3);
let strings = Array("a", "b", "c");

/* ES5 */
console.log("ES5 Result number array: ", numbers);
console.log("ES5 Result string array: ", strings);

/* var resultPush = numbers.push(...strings);
console.log("ES5 Result array: ", resultPush);
 */
var resultConcat = numbers.concat(strings);
console.log("ES5 Result array: ", resultConcat);

/* ES6 */

console.log("ES6 Result number array: ", numbers);
console.log("ES6 Result string array: ", strings);

let result = [...numbers, ...strings];
console.log("ES6 Result array: ", result);

// ------------------------------------

/* ES9 ECMAScript 2018 */

const person = { name: "Rick", age: 26 };

const persons = {
  ...person,
  country: "USA",
};

console.log(persons);
