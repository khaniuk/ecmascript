// Json parse

let persons = `[{
    "name": "Rick",
    "age": 35
  },
  {
    "name": "Karl",
    "age": 20
  },
  {
    "name": "Judith",
    "age": 10
  }
]`;

// String to Array
let result = JSON.parse(persons);
console.log(result);
