// var, let and const

/* ES5 */

var name = "MiYo";
console.log(name);
console.log("please not use var (is deprecate)");

/* ES6 */

let age = 25;
console.log(age);

// example
var x = 2;
{
  var x = 4;
  let age = 30;
  let phone = 123456789;
}
console.log(x, age); //phone is not defined

const YEAR = 2000;
console.log(YEAR);

// example 1
const numbers = Array(1, 2, 3, 4, 5);
console.log(numbers);

numbers.push(6);
console.log(numbers);

// example 1
const persons = Array(
  { name: `Rick`, age: 30, phone: 123456789 },
  { name: `Carl`, age: 20, phone: 234567890 }
);
console.log(persons);

persons[0].age = 35;
console.log(persons);
