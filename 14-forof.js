// forIn

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
];

/* ES5 */

for (const person of persons) {
  console.log(person.name);
}
