// Async Await

/* ES8 ECMAScript 2019 */

const hello = (name) => {
  return new Promise((resolve, reject) => {
    name.length > 0
      ? setTimeout(() => resolve(`Hello ${name}`), 1000)
      : reject(new Error("Not found param"));
  });
};

const useAsync = async (name) => {
  try {
    const result = await hello(name);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
};

useAsync("Rick");
