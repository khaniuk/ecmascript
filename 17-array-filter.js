// Array filter

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5 */

// Filters all elements that meet the condition
let result = persons.filter((person) => person.age < 18);
console.log(result);

/* ES6 */

// Check 22-array-find.js
