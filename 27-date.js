// Date

let currentDate = new Date();
console.log(currentDate); // format date

// Convert date to string, using the ISO standard format
let resultIso = currentDate.toISOString();
console.log(resultIso);

// Convert date to string, JSON dates have the same format as the ISO-8601 standard: YYYY-MM-DDTHH:mm:ss.sssZ
let resultJson = currentDate.toJSON();
console.log(resultJson);

// Other string functions
let resultSlice = currentDate.toISOString().slice(0, 10);
console.log(resultSlice);

// Result custom format date in string
// split string to array
// reverse array
// join array elements to string
let resultFormat = resultSlice.split("-").reverse().join("/");
console.log(`Format custom Date: ${resultFormat}`);

// DateTimeFormat
let dateL10nEN = new Intl.DateTimeFormat("en-US");
console.log(`Format US Date: ${dateL10nEN.format()}`);

let dateL10nES = new Intl.DateTimeFormat("es-ES");
console.log(`Format ES Date: ${dateL10nES.format()}`);

// OTHER FORMATS
// Number Formating
let numberL10nEN = new Intl.NumberFormat("en-US");
let numberL10nBO = new Intl.NumberFormat("es-BO");
console.log(numberL10nEN.format(1234567.89));
console.log(numberL10nBO.format(1234567.89));

// Currency format
let currencyL10nUSD = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});
let currencyL10nBOB = new Intl.NumberFormat("es-BO", {
  style: "currency",
  currency: "BOB",
});
console.log(`USD Currency: ${currencyL10nUSD.format(100200300.4)}`);
console.log(`BOB Currency: ${currencyL10nBOB.format(100200300.4)}`);
