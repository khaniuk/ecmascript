// Match with regex

/* ES9 ECMAScript 2020 */

const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;

const match = regexData.exec("2022-08-08");
console.log(match);

const year = match[1];
const month = match[2];
const day = match[3];

console.log(year, month, day);
