// Array some

const persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

/* ES5 */

// Check if some age are over 18
let result = persons.some((person) => person.age > 18);
console.log(result);
