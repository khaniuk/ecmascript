// Array flat

/* ES10 ECMAScript 2019 */

let numbers = Array(1, 2, 3, Array(6, 5, 4, Array(7, 8, 9)));
let depth = 2;
console.log(numbers.flat(depth));

numbers = [1, 2, 3, 4, 5];
console.warn(numbers.flatMap((value) => [value, value * 2]));
