// Entries and Values

/* ES8 ECMAScript 2017 */

const data = {
  frontend: "Rick",
  backend: "Karl",
  design: "Judith",
};

// Entries return array in array
const entries = Object.entries(data);
console.log(entries);
console.log(entries.length);

// Values return array
const values = Object.values(data);
console.log(values);

// ------------------------------------

/* ES10 ECMAScript 2019 */

let arrayEntries = [
  ["name", "Rick"],
  ["age", 26],
];
console.log(arrayEntries);
console.log(Object.fromEntries(arrayEntries));
