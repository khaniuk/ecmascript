// Includes

/* ES7 ECMAScript 2016 */

let text = "Hello world, welcome to the ECMAScript";
let result = text.includes("world"); // Returns true
console.log(`Result in string: ${result}`);

// ------------------------------------

const person = {
  name: "Judith",
  age: 10,
};

let persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

result = persons.includes(person);
console.log(`Result in array object (element out): ${result}`);

// ------------------------------------

persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  person,
];

result = persons.includes(person);
console.log(`Result in array object (element in): ${result}`);

// Check String.startsWith() and String.endsWith()
