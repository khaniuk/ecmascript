# ECMAScript

## Description

Test and example ECMAScript

- [ ] ES6 - 2015
- [ ] ES7
- [ ] ES8
- [ ] ES9

## Getting started

Install Nodejs

[Node.js official site](https://nodejs.org/en/)

Check node and npm version

```
node -v
npm -v
```

Run file example.js

```
node example.js
```

## License

For open source projects, [MIT](https://choosealicense.com/licenses/mit/)
