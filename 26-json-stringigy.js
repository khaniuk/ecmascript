// Json stringify

let persons = [
  {
    name: "Rick",
    age: 35,
  },
  {
    name: "Karl",
    age: 20,
  },
  {
    name: "Judith",
    age: 10,
  },
];

// Array to String
let result = JSON.stringify(persons);
console.log(result);
